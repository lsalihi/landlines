package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

/**
 * The persistent class for the s_typemessages database table.
 * 
 */
@Entity
@Table(name = "r_typemessages")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TypeMessage extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "tmes_id")
	private String id;

	/** The description. */
	@Column(name = "mes_description")
	private String description;

	/** The criticite. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tmes_criticite", referencedColumnName = "cod_id")
	private Codification criticite;

	/** The titre. */
	@Column(name = "tmes_titre")
	private String titre;

	/** The type. */
	@Column(name = "tmes_description")
	private String type;

	/** The code fonction. */
	@Column(name = "tmes_cod_fonction")
	private String codeFonction;

}