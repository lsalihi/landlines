
package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

@Entity
@Table(name = "r_devise")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Devise extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6494065577412840166L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PkGen")
	@Column(name = "dev_id")
	private Integer id;

	/** The abreviation courte. */
	@Column(name = "dev_abreviation_courte")
	private String abreviationCourte;

	/** The abreviation longue. */
	@Column(name = "dev_abreviation_longue")
	private String abreviationLongue;

	/** The code. */
	@Column(name = "dev_code")
	private String code;

	/** The libelle. */
	@Column(name = "dev_libelle")
	private String libelle;

	/** The nombre apres virgule. */
	@Column(name = "dev_nombre_apres_virgule")
	private int nombreApresVirgule;

	/** The statut. */
	@Column(name = "dev_actif")
	private boolean actif;
}