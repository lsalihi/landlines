/*
 * ModeReglement.java
 *
 * Copyright (c) 1990-2015 M2M Group, Inc. All Rights Reserved.
 */
package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

@Entity
@Table(name = "r_mode_reglement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ModeReglement  extends AbstractAuditingEntity implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 690401655853753675L;

	/** The id. */
	@Id
	@SequenceGenerator(name = "seq_mode_reglement", sequenceName = "seq_mode_reglement", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mode_reglement")
	@Column(name = "mr_id")
	private Long id;

	/** Libell� du mode de reglement */
	@Column(name = "mr_libelle", length = 30)
	private String libelle;

	/** Description du mode de reglement. */
	@Column(name = "mr_description", length = 300)
	private String description;


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="mr_code")
	private Codification modeReglement;
	
	/** Statut du mode de reglement. */
	@Column(name = "mr_actif", length = 300)
	private boolean actif;


}
