package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

/**
 * The persistent class for the "R_Codification" database table.
 * 
 */
@Entity
@Table(name = "r_codification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Codification extends AbstractAuditingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "codification_id_seq", sequenceName = "codification_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "codification_id_seq")
	@Column(name = "cod_id")
	private Long id;

    @NotNull
    @Size(max = 50)
	@Column(name = "cod_table")
	private String codeTable;

    @NotNull
    @Size(max = 50)
	@Column(name = "cod_codification")
	private String codeCodification;

	@Column(name = "cod_libelle")
	private String codeLibelle;

	@Column(name = "cod_actif")
	private boolean codeActif;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodeTable() {
		return codeTable;
	}

	public void setCodeTable(String codeTable) {
		this.codeTable = codeTable;
	}

	public String getCodeCodification() {
		return codeCodification;
	}

	public void setCodeCodification(String codeCodification) {
		this.codeCodification = codeCodification;
	}

	public String getCodeLibelle() {
		return codeLibelle;
	}

	public void setCodeLibelle(String codeLibelle) {
		this.codeLibelle = codeLibelle;
	}

	public Boolean getCodeActif() {
		return codeActif;
	}

	public void setCodeActif(Boolean codeActif) {
		this.codeActif = codeActif;
	}

}