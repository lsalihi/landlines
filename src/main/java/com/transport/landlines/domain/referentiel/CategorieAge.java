/*
 * CategorieAge.java
 *
 * Copyright (c) 1990-2015 M2M Group, Inc. All Rights Reserved.
 */
package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

@Entity
@Table(name = "r_categorie_age")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CategorieAge extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8818936659673700786L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_categorie_age")
	@SequenceGenerator(name = "seq_categorie_age", sequenceName = "seq_categorie_age", allocationSize = 1)
	@Column(name = "cag_id")
	private Long id;

	/** The libelle. */
	@Column(name = "cag_libelle", length = 30)
	private String libelle;

	/** The description. */
	@Column(name = "cag_description", length = 255)
	private String description;

	/** The activite metier. */
	@Column(name = "cag_activite_metier")
	private Codification activiteMetier;

	/** The min. */
	@Column(name = "cag_min")
	private Long min;

	/** The max. */
	@Column(name = "cag_max")
	private Long max;

}
