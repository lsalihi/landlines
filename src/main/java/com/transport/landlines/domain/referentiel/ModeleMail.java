package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

/**
 * The persistent class for the r_modelemail database table.
 * 
 */
@Entity
@Table(name = "r_modele_mail")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ModeleMail extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_modele_mail")
	@SequenceGenerator(name = "seq_modele_mail", sequenceName = "seq_modele_mail", allocationSize = 1)
	@Column(name = "mail_id")
	private Integer id;

	/** The actif. */
	@Column(name = "mai_actif")
	private boolean actif;

	/** The corps mail. */
	@Column(name = "mai_corpsmail")
	private String corpsMail;

	/** The destinataire. */
	@Column(name = "mai_destinataire")
	private String destinataire;

	/** The objet mail. */
	@Column(name = "mai_objetmail")
	private String objetMail;

	/** The seuil. */
	@Column(name = "mai_seuil")
	private Integer seuil;

	/** The type message. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mai_typemail")
	private TypeMessage typeMessage;

}