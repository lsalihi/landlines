package com.transport.landlines.domain.referentiel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.UpdateTimestamp;

import com.transport.landlines.domain.AbstractAuditingEntity;

/**
 * The persistent class for the m_notifications database table.
 * 
 */
@Entity
@Table(name = "m_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Notification extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_notification")
	@SequenceGenerator(name = "seq_notification", sequenceName = "seq_notification", allocationSize = 1)
	@Column(name = "notif_id")
	private Integer id;

	/** The date creation. */
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "notif_date_creation")
	private Date dateCreation;

	/** The id organisation. */
	@Column(name = "user_id")
	private String userId;

	/** The type message. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "notif_typemsg", referencedColumnName = "tmes_id")
	private TypeMessage typeMessage;

}