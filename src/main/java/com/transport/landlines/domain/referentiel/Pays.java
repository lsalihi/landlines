/*
 * Pays.java
 *
 * Copyright (c) 1990-2015 M2M Group, Inc. All Rights Reserved.
 */
package com.transport.landlines.domain.referentiel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

@Entity
@Table(name = "r_pays")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pays extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4580861121219077034L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pays")
	@SequenceGenerator(name = "seq_pays", sequenceName = "seq_pays", allocationSize = 1)
	@Column(name = "pys_id")
	private Long id;

	/** The abreviation. */
	@Column(name = "pys_abreviation")
	private String abreviation;

	/** The code alphanum. */
	@Column(name = "pys_code_alphanum")
	private String codeAlphanum;

	/** The code numerique. */
	@Column(name = "pys_code_numerique")
	private String codeNumerique;

	/** The date debut heure ete. */
	@Column(name = "pys_debut_heure_ete")
	@Temporal(TemporalType.DATE)
	private Date dateDebutHeureEte;

	/** The date fin heure ete. */
	@Column(name = "pys_fin_heure_ete")
	@Temporal(TemporalType.DATE)
	private Date dateFinHeureEte;

	/** The heure ete. */
	@Column(name = "pys_heure_ete")
	private Boolean heureEte;

	/** The decalage horaire. */
	@Column(name = "pys_decalage_horaire")
	private String decalageHoraire;

	/** The libelle. */
	@Column(name = "pys_libelle")
	private String libelle;

	/** The devise. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pys_devise", referencedColumnName = "dev_id")
	private Devise devise;

	@Column(name = "pys_actif")
	private boolean actif;

}