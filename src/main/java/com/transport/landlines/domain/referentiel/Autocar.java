/*
 * Autocar.java
 *
 * Copyright (c) 1990-2015 M2M Group, Inc. All Rights Reserved.
 */
package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

@Entity
@Table(name = "r_autocar")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Autocar extends AbstractAuditingEntity implements Serializable {

	/** Le/La Constant serialVersionUID. */
	private static final long serialVersionUID = -7512506227969215393L;

	/** Le/La id. */
	@Id
	@SequenceGenerator(name = "seq_autocar", sequenceName = "seq_autocar", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_autocar")
	@Column(name = "auto_id")
	private Integer id;

	/** Le/La numero societe. */
	@Column(name = "auto_numero_societe")
	private String numeroSociete;

	/** Le/La marque. */
	@Column(name = "auto_marque")
	private String marque;

	/** Le/La immatriculation. */
	@Column(name = "auto_immatriculation")
	private String immatriculation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "auto_categorie", referencedColumnName = "cod_id")
	private Codification categorieAutocar;

	/** Le/La capacite. */
	@Column(name = "auto_capacite")
	private Integer capacite;

	/** Le/La statut. */
	@Column(name = "auto_actif")
	private boolean actif;

}
