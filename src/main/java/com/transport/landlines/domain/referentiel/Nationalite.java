package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

/**
 * A nationalite.
 */
@Entity
@Table(name = "r_nationalite")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Nationalite extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@SequenceGenerator(name = "nationalite_id_seq", sequenceName = "nationalite_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nationalite_id_seq")
	@Column(name = "nat_id")
	private Long id;

	/** The libelle. */
	@Column(name = "nat_libelle", length = 20)
	private String libelle;

	@Column(name = "nat_actif")
	private boolean actif;

}
