/*
 * Ville.java
 *
 * Copyright (c) 1990-2015 M2M Group, Inc. All Rights Reserved.
 */
package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

@Entity
@Table(name = "r_ville")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Ville extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8007912850523032081L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_r_ville")
	@SequenceGenerator(name = "s_r_ville", sequenceName = "s_r_ville", allocationSize = 1)
	@Column(name = "vil_id")
	private Long id;

	/** The libelle. */
	@Column(name = "vil_libelle")
	private String libelle;

	/** The pays. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vil_pays", referencedColumnName = "pys_id")
	private Pays pays;


	@Column(name = "cod_id")
	private boolean actif;

}
