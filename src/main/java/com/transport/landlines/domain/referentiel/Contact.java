package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

/**
 * The persistent class for the r_contacts database table.
 * 
 */
@Entity
@Table(name = "r_contact")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Contact extends AbstractAuditingEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_contact")
	@SequenceGenerator(name = "seq_contact", sequenceName = "seq_contact", allocationSize = 1)
	@Column(name = "cnt_id")
	private Integer id;

	/** The email. */
	@Column(name = "cnt_adrmail")
	private String email;

	/** The commentaire. */
	@Column(name = "cnt_commentaire")
	private String commentaire;

	/** The nom. */
	@Column(name = "cnt_nom")
	private String nom;

	/** The prenom. */
	@Column(name = "cnt_prenom")
	private String prenom;

	/** The principal. */
	@Column(name = "cnt_principal")
	private Boolean principal;

	/** The tel fixe. */
	@Column(name = "cnt_telfixe")
	private String telFixe;

	/** The tel portable. */
	@Column(name = "cnt_telportable")
	private String telPortable;

}