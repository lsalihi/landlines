///*
// * Agence.java
// *
// * Copyright (c) 1990-2017 M2M Group, Inc. All Rights Reserved.
// */
//package com.transport.landlines.domain.referentiel;
//
//import java.io.Serializable;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Set;
//
//import javax.persistence.Cacheable;
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.EnumType;
//import javax.persistence.Enumerated;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.JoinTable;
//import javax.persistence.ManyToMany;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;
//import javax.persistence.OrderBy;
//import javax.persistence.Table;
//import javax.persistence.TableGenerator;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//import org.aspectj.weaver.loadtime.Agent;
//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.m2m.mxmove.business.authorization.model.CustomUser;
//import com.m2m.mxmove.business.common.AgenceBusinessStatus;
//import com.m2m.mxmove.business.referentiel.vo.VoGroupsInterfaces.AgenceWizard.AgcWizardStep;
//import com.m2m.technicalservices.dao.EntityStatus;
//import com.transport.landlines.domain.AbstractAuditingEntity;
//
///**
// * The Class Agence.
// *
// * @version 1.0 13 mars 2015
// * @author amtarji
// */
//@Entity
//@Table(name = "REF_AGENCE")
//@TableGenerator(name = "PkGen", table = "sequence_generator", pkColumnName = "GEN_NAME", pkColumnValue = "Agence_Gen", valueColumnName = "GEN_VALUE", initialValue = 0, allocationSize = 1)
//@Cacheable
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "referentiel")
//public class Agence extends AbstractAuditingEntity implements Serializable {
//
//	/** The Constant serialVersionUID. */
//	private static final long serialVersionUID = -1942356177364828477L;
//
//	/** The id. */
//	@Id
//	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PkGen")
//	@Column(name = "AGC_ID")
//	private Long id;
//
//	/** The point passage. */
//	@Column(name = "AGC_POINT_PASSAGE")
//	private Boolean pointPassage;
//
//	/** The libelle. */
//	@Column(name = "AGC_LIBELLE")
//	private String libelle;
//
//	/** The code. */
//	@Column(name = "AGC_CODE")
//	private String code;
//
//	/** The date creation. */
//	@Column(name = "AGC_DATE_CREAT")
//	private Date dateCreation;
//
//	/** The code. */
//	@Column(name = "AGC_CODE_COMPT")
//	private String codeComptable;
//
//	/** The taxe professionnelles. */
//	@Column(name = "AGC_TAXE_PROF")
//	private Double taxeProfessionnelles;
//
//	/** The identification fiscale. */
//	@Column(name = "AGC_ID_FISCALE")
//	private String identificationFiscale;
//
//	/** The chef agence. */
//	@ManyToOne
//	@JoinColumn(name = "AGC_CHEF_AGENCE", referencedColumnName = "USERNAME")
//	private CustomUser chefAgence;
//
//	/** The responsable. */
//	@ManyToOne
//	@JoinColumn(name = "AGC_RESPONSABLE", referencedColumnName = "USERNAME")
//	private CustomUser responsable;
//
//	/** The canal vente. */
//	@ManyToOne
//	@JoinColumn(name = "AGC_CANAL_VENTE", referencedColumnName = "CV_ID")
//	@JsonIgnore
//	private CanalVente canalVente;
//
//	/** The profil canal vente. */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "AGC_PROFIL_CV", referencedColumnName = "PCV_ID")
//	private ProfilCanalVente profilCanalVente;
//
//	/** The debut service. */
//	@Column(name = "AGC_DEBUT_SERV")
//	@Temporal(TemporalType.TIME)
//	private Date debutService;
//
//	/** The fin service. */
//	@Column(name = "AGC_FIN_SERVICE")
//	@Temporal(TemporalType.TIME)
//	private Date finService;
//
//	/** The adresse1. */
//	@Column(name = "AGC_ADRESSE_1")
//	private String adresse1;
//
//	/** The adresse2. */
//	@Column(name = "AGC_ADRESSE_2")
//	private String adresse2;
//
//	/** The code postal. */
//	@Column(name = "AGC_CODE_POST")
//	private String codePostal;
//
//	/** The email. */
//	@Column(name = "AGC_EMAIL")
//	private String email;
//
//	/** The fax. */
//	@Column(name = "AGC_FAX")
//	private String fax;
//
//	/** The ville. */
//	@ManyToOne
//	@JoinColumn(name = "AGC_VILLE", referencedColumnName = "VIL_ID")
//	private Ville ville;
//
//	/** The principale ville. */
//	@Column(name = "AGC_PRINCIPALE_VIL")
//	private Boolean principaleVille;
//
//	/** The date ouverture. */
//	@Column(name = "AGC_DATE_OUV")
//	private Date dateOuverture;
//
//	/** The date mise en prod. */
//	@Column(name = "AGC_DATE_PROD")
//	private Date dateMiseEnProd;
//
//	/** The mode reglements. */
//	@ManyToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "REF_AGC_MR", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(nullable = true, name = "MR_ID", referencedColumnName = "MR_ID") })
//	private Set<ModeReglement> modeReglements;
//
//	/** The fond caisse obligatoire. */
//	@Column(name = "AGC_FOND_CAIS")
//	private Double fondCaisseObligatoire;
//
//	/** The seuil versement intermediaire. */
//	@Column(name = "AGC_SEUIL_VERS")
//	private Double seuilVersementIntermediaire;
//
//	/** The valide. */
//	@Column(name = "AGC_VALIDE")
//	private Boolean valide;
//
//	/** The informatise. */
//	@Column(name = "AGC_INFO")
//	private Boolean informatise;
//
//	/** The support lecture auto. */
//	@Column(name = "AGC_SUPP_LECT")
//	private Boolean supportLectureAuto;
//
//	/** The point embarquement. */
//	@Column(name = "AGC_PT_EMBARQ")
//	private Boolean pointEmbarquement;
//
//	/** The point debarquement. */
//	@Column(name = "AGC_PT_DEBARQ")
//	private Boolean pointDebarquement;
//
//	/** The correspondance. */
//	@Column(name = "AGC_CORRESP")
//	private Boolean correspondance;
//
//	/** The principale secteur. */
//	@Column(name = "AGC_PRINCIP_SECT")
//	private Boolean principaleSecteur;
//
//	/** The libelle. */
//	@Column(name = "AGC_COMPTE_RIB", length = 30)
//	private String rib;
//
//	/** The agence. */
//	@Column(name = "AGC_COMPTE_AGENCE", length = 30)
//	private String compteAgence;
//
//	/** The responsable hors maroc. */
//	@Column(name = "AGC_RESPONSABLE_HORS_MAROC", length = 30)
//	private String responsableHorsMaroc;
//
//	/** The telephone. */
//	@Column(name = "AGC_TELEPHONE")
//	private String telephone;
//
//	/** The ville. */
//	@ManyToOne
//	@JoinColumn(name = "AGC_COMPTE_VILLE", referencedColumnName = "VIL_ID")
//	private Ville villeCompte;
//
//	/** The banque. */
//	@ManyToOne
//	@JoinColumn(name = "AGC_BANQUE", referencedColumnName = "BQ_ID")
//	private Banque banque;
//
//	/** The adresse1 agence. */
//	@Column(name = "AGC_COMPT_ADRS_1", length = 100)
//	private String adresse1CompteAgence;
//
//	/** The adresse2 agence. */
//	@Column(name = "AGC_COMPT_ADRS_2", length = 100)
//	private String adresse2CompteAgence;
//
//	/** The code postal agence. */
//	@Column(name = "AGC_COMPT_CODE_POST", length = 30)
//	private String codePostalCompteAgence;
//
//	/** The compte compagnie. */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "AGC_CMPTE_COMP", referencedColumnName = "CB_ID")
//	private CompteBancaire compteCompagnie;
//
//	/** The horaires services. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID")
//	private Set<HoraireService> horairesServices;
//
//	/** The historique statuts. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID")
//	@OrderBy("dateChangement DESC")
//	private Set<HistoriqueStatutAgence> historiqueStatuts;
//
//	/** The telephones. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinTable(name = "REF_AGC_TELS", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "TEL_ID", referencedColumnName = "TEL_ID") })
//	private Set<Telephone> telephones;
//
//	/** The recettes previsionelles iu. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinTable(name = "REF_AGC_RPIU", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "RP_ID", referencedColumnName = "RP_ID") })
//	private Set<RecettePrevisionnelleInterurbaine> recettesPrevisionellesIU;
//
//	/** The recettes previsionelles in. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinTable(name = "REF_AGC_RPIN", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "RP_ID", referencedColumnName = "RP_ID") })
//	private Set<RecettePrevisionnelleInternationale> recettesPrevisionellesIN;
//
//	/** The deductions. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinTable(name = "REF_AGC_DED", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "DED_ID", referencedColumnName = "DED_ID") })
//	private Set<Deduction> deductions;
//
//	/** The materiels informatiques. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinTable(name = "REF_AGC_MAT", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "MTINF_ID", referencedColumnName = "MTINF_ID") })
//	private Set<MaterielInformatique> materielsInformatiques;
//
//	/** The pieces jointes. */
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinTable(name = "REF_AGC_PJ", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "PJ_ID", referencedColumnName = "PJ_ID") })
//	private Set<PieceJointe> piecesJointes;
//
//	/** The agents. */
//	@OneToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "REF_AGC_AG", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "AG_ID", referencedColumnName = "AG_ID") })
//	private Set<Agent> agents;
//
//	/** The custom users. */
//	@OneToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "REF_AGC_UC", joinColumns = { @JoinColumn(name = "AGC_ID", referencedColumnName = "AGC_ID") }, inverseJoinColumns = { @JoinColumn(name = "username", referencedColumnName = "username") })
//	private Set<CustomUser> customUsers;
//
//	/** The etape. */
//	@Column(name = "AGC_ETAPE")
//	@Enumerated(EnumType.STRING)
//	private AgcWizardStep etape;
//
//	/** The statut. */
//	@Column(name = "AGC_STATUT_MET")
//	@Enumerated(EnumType.STRING)
//	private AgenceBusinessStatus statutMetier;
//
//	/** The statut. */
//	@Column(name = "AGC_STATUT")
//	@Enumerated(EnumType.STRING)
//	private EntityStatus statut;
//
//	/** The date desactivation. */
//	@Column(name = "AGC_DATE_DESA")
//	private Date dateDesactivation;
//
//	/** The motif desactivation. */
//	@Column(name = "AGC_MOTIF_DESAC")
//	private String motifDesactivation;
//
//	/** The motif fermeture. */
//	@Column(name = "AGC_MOTIF_FERM")
//	private String motifFermeture;
//
//	/** The motif reouverture. */
//	@Column(name = "AGC_MOTIF_REOUV")
//	private String motifReouverture;
//
//	/** The motif statut metier. */
//	@Column(name = "AGC_MOTIF_STAT_MET")
//	private String motifStatutMetier;
//
//	/** The motif statut. */
//	@Column(name = "AGC_MOTIF_STAT")
//	private String motifStatut;
//
//	/** The date fermeture. */
//	@Column(name = "AGC_DATE_FERM")
//	// @Temporal(TemporalType.DATE)
//	private Date dateFermeture;
//
//	/** The fondDepense. */
//	@Column(name = "AGC_DEPENSE_FOND")
//	private Double fondDepense;
//
//	/** The Depense Saisie. */
//	@Column(name = "AGC_SAISIE_DEP")
//	private Boolean saisieDepense;
//
//	/**
//	 * The Montant prepaye: montant pr�pay� par l'agence dans le cas d'une
//	 * agence commisionn�e avec type de commision pr�pay�.
//	 */
//	@Column(name = "AGC_MONATANT_PRE_PAYE")
//	private Double montantPrePaye;
//
//	/** The Montant d�duction total. */
//	@Column(name = "AGC_MONATANT_DEDUCTION_TOTALE")
//	private Double montantDeductionTotale;
//
//	/** The Montant d�duction pay�. */
//	@Column(name = "AGC_MONATANT_DEDUCTION_PAYE")
//	private Double montantDeductionPaye;
//
//	/** The Montant d�duction restant. */
//	@Column(name = "AGC_MONATANT_DEDUCTION_RESTANT")
//	private Double montantDeductionRest;
//
//	/** pour des agences commissionaire virement. */
//	@Column(name = "AGC_BLOQUE_COMMISSION")
//	private Boolean activeCommission;
//
//	/**
//	 * pour des agences dont la vente INTERNATIONAL ne change que la
//	 * disponibilite Nord (disponibilite europe).
//	 */
//	@Column(name = "AGC_DISPO_NORD")
//	private Boolean changeDispoNord;
//
//}