/*
 * KmInterVille.java
 *
 * Copyright (c) 1990-2015 M2M Group, Inc. All Rights Reserved.
 */
package com.transport.landlines.domain.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.transport.landlines.domain.AbstractAuditingEntity;

@Entity
@Table(name = "r_km_inter_ville")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class KmInterVille extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = -8293293902784454696L;

	@Id
	@SequenceGenerator(name = "seq_km_inter_ville", sequenceName = "seq_km_inter_ville", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_km_inter_ville")
	@Column(name = "kiv_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "kiv_ville_depart")
	private Ville villeDepart;

	@ManyToOne
	@JoinColumn(name = "kiv_ville_arrivee")
	private Ville villeArrivee;

	/** Kilometres. */
	@Column(name = "kiv_kilometres")
	private int kilometres;

	/** The statut. */
	@Column(name = "kiv_actif")
	private boolean statut;

}
