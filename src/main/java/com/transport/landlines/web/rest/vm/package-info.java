/**
 * View Models used by Spring MVC REST controllers.
 */
package com.transport.landlines.web.rest.vm;
