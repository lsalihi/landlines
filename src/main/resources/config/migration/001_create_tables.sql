SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT
   pg_catalog.set_config

   (
      'search_path',
      '',
      false
   );
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET search_path= public;
SET default_tablespace = '';
SET default_with_oids = false;
   --  Create Sequences
CREATE SEQUENCE s_codifications START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
   --@annotaion
   --@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_R_IMMEUBLE")
   --@SequenceGenerator(name = "SEQ_R_IMMEUBLE", sequenceName = "SEQ_R_IMMEUBLE", allocationSize = 1)
   -- table codification
CREATE TABLE r_codification
   (
      cod_id integer NOT NULL,
      cod_idTable character varying (20) NOT NULL,
      cod_codification character varying (10) NOT NULL,
      cod_libelle character varying (255),
      cod_libelle_court character varying (100),
      cod_actif boolean
   );
ALTER TABLE ONLY r_codification ADD CONSTRAINT "pk_r_codification" PRIMARY KEY (cod_id);
/** create table codification */    -- table nationalie
CREATE SEQUENCE nationalite_id_seq INCREMENT 1 START 1;
CREATE TABLE r_nationalite
   (
      nat_id bigint NOT NULL,
      nat_libelle character varying (50),
      nat_actif boolean NOT NULL,
      cod_libelle character varying (50),
      CONSTRAINT r_nationalite_pkey PRIMARY KEY (nat_id),
      CONSTRAINT r_nationalite_unique UNIQUE (nat_libelle)
   );
   -- table devise
CREATE SEQUENCE s_devises START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_devise
   (
      dev_id integer NOT NULL,
      dev_abreviation_courte character varying (10) NOT NULL,
      dev_abreviation_longue character varying (10) NOT NULL,
      dev_code character varying (10),
      dev_libelle character varying (25),
      dev_nombre_apres_virgule integer default 2,
      dev_actif boolean NOT NULL default true,
      CONSTRAINT r_devise_pkey PRIMARY KEY (dev_id)
   );
   -- table pays
CREATE SEQUENCE seq_pays START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_pays
   (
      pys_id integer NOT NULL,
      pys_abreviation character varying (10) NOT NULL,
      pys_code_alphanum character varying (10) NOT NULL,
      pys_code_numerique character varying (10),
      pys_decalage_horaire character varying (25),
      pys_debut_heure_ete timestamp with time zone NULL,
      pys_fin_heure_ete timestamp with time zone NULL,
      pys_heure_ete boolean default false,
      pys_libelle character varying (50),
      pys_devise integer NOT NULL,
      pys_actif boolean NOT NULL,
      CONSTRAINT r_pays_pkey PRIMARY KEY (pys_id),
      CONSTRAINT r_pays_devise_fkey FOREIGN KEY (pys_devise) REFERENCES r_devise (dev_id)
   );
-- table categorie autocar ! repmalcer par codifcation
-- CREATE SEQUENCE seq_categorie_autocar START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
--CREATE TABLE r_categorie_autocar
--   (
--      cauto_id integer NOT NULL,
--      cauto_libelle character varying (30) NOT NULL,
--      cauto_description integer NOT NULL,
--      cauto_actif boolean NOT NULL,
--      CONSTRAINT r_categorie_autocar_pkey PRIMARY KEY (cauto_id)
--   );  --
   -- table autocar
CREATE SEQUENCE seq_autocar START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_autocar
   (
      auto_id integer NOT NULL,
      auto_numero_societe character varying (50) NOT NULL,
      auto_marque character varying (50) NOT NULL,
      auto_immatriculation character varying (50) NOT NULL,
      auto_categorie integer NOT NULL,
      -- codification
      auto_capacite integer NOT NULL,
      auto_actif boolean NOT NULL,
      CONSTRAINT r_autocar_pkey PRIMARY KEY (auto_id),
      CONSTRAINT r_autocar_categorie_fkey FOREIGN KEY (auto_categorie) REFERENCES r_codification (cod_id)
   );
   -- categorie age
CREATE SEQUENCE seq_categorie_age START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_categorie_age
   (
      cag_id integer NOT NULL,
      cag_libelle character varying (50) NOT NULL,
      cag_description character varying (255),
      cag_min integer NOT NULL,
      cag_max integer NOT NULL,
      cag_activite_metier integer NOT NULL,
      cag_actif boolean NOT NULL,
      CONSTRAINT r_categorie_age_pkey PRIMARY KEY (cag_id),
      CONSTRAINT r_age_activite_fkey FOREIGN KEY (cag_activite_metier) REFERENCES r_codification (cod_id)
   );
   --  KmInterVille
CREATE SEQUENCE seq_km_inter_ville START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_km_inter_ville
   (
      kiv_id integer NOT NULL,
      kiv_libelle character varying (50),
      kiv_ville_depart integer NOT NULL,
      kiv_ville_arrivee integer NOT NULL,
      kiv_kilometres integer NOT NULL,
      cag_actif boolean NOT NULL default true,
      CONSTRAINT r_km_inter_ville_pkey PRIMARY KEY (kiv_id),
      CONSTRAINT r_ki_ville_dep_fkey FOREIGN KEY (kiv_ville_depart) REFERENCES r_ville(vil_id),
      CONSTRAINT r_ki_ville_arr_fkey FOREIGN KEY (kiv_ville_arrivee) REFERENCES r_ville(vil_id)
   );
   -- telefon
CREATE SEQUENCE seq_contact START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_contact
   (
      cnt_id integer NOT NULL,
      cnt_nom character varying (255),
      cnt_prenom character varying (255),
      cnt_telfixe character varying (10),
      cnt_telportable character varying (10),
      cnt_adrmail character varying (500),
      cnt_commentaire character varying (255),
      cnt_principal boolean NOT NULL
   );
ALTER TABLE ONLY r_contact ADD CONSTRAINT pk_r_contacts PRIMARY KEY (cnt_id);
   -- table societe
CREATE SEQUENCE s_societe START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_societe
   (
      sct_id integer NOT NULL,
      sct_raison_sociale character varying (50) NOT NULL,
      sct_email_res character varying (100),
      sct_adresse_siege character varying (255),
      sct_description character varying (100),
      sct_contact integer NOT NULL,
      -- list contact,
      sct_actif boolean NOT NULL,
      CONSTRAINT r_societe_pkey PRIMARY KEY (sct_id),
      CONSTRAINT r_societe_contact_fkey FOREIGN KEY (sct_contact) REFERENCES r_contact (cnt_id)
   );
   -- banque
CREATE SEQUENCE s_banque START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_banque
   (
      bq_id integer NOT NULL,
      bq_libelle character varying (100),
      bq_abreviation character varying (30),
      bq_description character varying (255),
      bq_actif boolean NOT NULL,
      bq_societe integer NOT NULL,
      CONSTRAINT r_banque_pkey PRIMARY KEY (bq_id),
      CONSTRAINT r_banque_societe_fkey FOREIGN KEY (bq_societe) REFERENCES r_societe (sct_id)
   );
   -- horaire service
CREATE SEQUENCE s_horaire_services START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_horaire_service
   (
      hs_id integer NOT NULL,
      hs_ouverture timestamp with time zone NULL,
      hs_fermeture timestamp with time zone NULL,
      CONSTRAINT r_horaire_service_pkey PRIMARY KEY (hs_id)
   );
   -- materiel informatique
CREATE SEQUENCE s_materiel_info START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_materiel_info
   (
      mtinf_id integer NOT NULL,
      mtinf_reference character varying (100),
      mtinf_commentaire character varying (30),
      mtinf_type_equpement integer,
      -- codification
      mtinf_marque integer,
      -- codification,
      CONSTRAINT r_materiel_info_pkey PRIMARY KEY (mtinf_id),
      CONSTRAINT r_horaire_service_type_fkey FOREIGN KEY (mtinf_type_equpement) REFERENCES r_codification (cod_id),
      CONSTRAINT r_horaire_service_marque_fkey FOREIGN KEY (mtinf_marque) REFERENCES r_codification (cod_id)
   );
   -- table type message
CREATE TABLE r_typemessages
   (
      tmes_id character varying (10) NOT NULL,
      tmes_type character varying (50),
      tmes_titre character varying (100),
      tmes_description character varying (255),
      tmes_criticite character varying (10),
      tmes_cod_fonction character varying (20)
   );
ALTER TABLE ONLY r_typemessages ADD CONSTRAINT pk_r_typemessages PRIMARY KEY (tmes_id);

ALTER TABLE ONLY r_typemessages ADD CONSTRAINT r_type_msg_criticite_fkey FOREIGN KEY (tmes_criticite) REFERENCES r_codification (cod_id);

   -- table message
CREATE SEQUENCE seq_modele_mail START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_modele_mail
   (
      mail_id integer NOT NULL,
      tmes_id character varying (10),
      mai_typemail character varying (20) NOT NULL,
      mai_objetmail character varying (255) NOT NULL,
      mai_destinataire character varying (500),
      mai_corpsmail character varying (2000) NOT NULL,
      mai_seuil integer,
      mai_actif boolean DEFAULT true NOT NULL
   );
ALTER TABLE ONLY r_modele_mail ADD CONSTRAINT pkr_modele_mail PRIMARY KEY (mail_id);
ALTER TABLE ONLY r_modele_mail ADD CONSTRAINT r_type_msg_mail_fkey FOREIGN KEY (mai_typemail) REFERENCES r_typemessages (tmes_id);

   -- notification
CREATE SEQUENCE seq_notification START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE m_notification
   (
      notif_id integer NOT NULL,
      -- Id notification
      user_id varchar (10) NULL,
      notif_typemsg character varying (10),
      -- identifinat type message
      notif_date_creation timestamp with time zone NULL -- date de création
   );
ALTER TABLE ONLY m_notification ADD CONSTRAINT "pkm_notification" PRIMARY KEY (notif_id);
ALTER TABLE ONLY m_notification ADD CONSTRAINT r_type_msg_notif_fkey FOREIGN KEY (notif_typemsg) REFERENCES r_typemessages (tmes_id);

-- table ville
CREATE SEQUENCE s_r_ville START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_ville
   (
      vil_id integer NOT NULL,
      vil_libelle character varying (20) NOT NULL,
      vil_pays integer NOT NULL,
      vil_actif boolean DEFAULT true NOT NULL,
      CONSTRAINT r_ville_pkey PRIMARY KEY (vil_id),
      CONSTRAINT r_ville_pays_fkey FOREIGN KEY (vil_pays) REFERENCES r_pays (pys_id)
   );
  
  
-- table mode reglement
CREATE SEQUENCE seq_mode_reglement START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
CREATE TABLE r_mode_reglement
   (
      mr_id integer NOT NULL,
      mr_libelle character varying (30) NOT NULL,
      mr_description character varying (255),
      mr_code integer NOT NULL,
      mr_actif boolean DEFAULT true NOT NULL,
      CONSTRAINT r_mode_reglement_pkey PRIMARY KEY (mr_id),
      CONSTRAINT r_mode_relement_code_fkey FOREIGN KEY (mr_code) REFERENCES  r_codification (cod_id)
   );
